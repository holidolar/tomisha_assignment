import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';

class DescriptionText extends StatelessWidget {
  const DescriptionText({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.left,
      style: const TextStyle(
        color: ThemeColor.highlightedText,
        fontSize: 16,
      ),
    );
  }
}
