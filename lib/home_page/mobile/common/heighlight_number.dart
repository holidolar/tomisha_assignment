import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';

class HighlightedNumber extends StatelessWidget {
  const HighlightedNumber({
    Key? key,
    required this.number,
  }) : super(key: key);

  final int number;

  @override
  Widget build(BuildContext context) {
    return Text(
      '$number.',
      textAlign: TextAlign.center,
      style: const TextStyle(
        color: ThemeColor.highlightedText,
        fontSize: 130,
      ),
    );
  }
}
