import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/desciption_text.dart';
import 'package:tomisha/home_page/mobile/common/heighlight_number.dart';
// TabWidgetThree

class TabWidgetThree extends StatelessWidget {
  const TabWidgetThree({
    Key? key,
    required this.img,
    required this.text,
    required this.index,
  }) : super(key: key);

  final List<String> img;
  final List<String> text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 300,
      child: Stack(
        children: [
          const Positioned(
            top: -90,
            left: -120,
            child: CircleWidget(
              size: 390,
            ),
          ),
          Positioned(
            right: 50,
            bottom: 0,
            child: Image.asset(
              img[index - 1],
            ),
          ),
          Positioned(
            top: 5,
            left: 50,
            bottom: index == 1 ? 0 : 150,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                HighlightedNumber(
                  number: index,
                ),
                Positioned(
                  bottom: 55,
                  left: 120,
                  child: DescriptionText(
                    text: text[index],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
