import 'package:flutter/material.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/desciption_text.dart';
import 'package:tomisha/home_page/mobile/common/heighlight_number.dart';

class TabWidgetOne extends StatelessWidget {
  const TabWidgetOne({
    Key? key,
    required this.img,
    required this.text,
    required this.index,
  }) : super(key: key);

  final List<String> img;
  final List<String> text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 300,
      child: Stack(
        children: [
          const Positioned(
            bottom: -30,
            left: -40,
            child: CircleWidget(
              size: 250,
            ),
          ),
          Positioned(
            right: 50,
            top: 20,
            child: Image.asset(
              img[index - 1],
            ),
          ),
          Positioned(
            bottom: 0,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                HighlightedNumber(
                  number: index,
                ),
                Positioned(
                  bottom: 27,
                  left: 120,
                  child: DescriptionText(
                    text: text[index],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
