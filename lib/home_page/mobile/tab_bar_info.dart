import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/mobile/tab_widget.dart';
import 'package:animated_segmented_tab_control/animated_segmented_tab_control.dart';

class InfoTabMiddleSection extends StatelessWidget {
  InfoTabMiddleSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: SizedBox(
        height: 1050,
        width: double.infinity,
        child: Stack(
          children: [
            const Padding(
              padding: EdgeInsets.all(16.0),
              child: SegmentedTabControl(
                radius: Radius.circular(10),
                backgroundColor: Colors.white,
                indicatorColor: ThemeColor.tabActive,
                tabTextColor: ThemeColor.tabLabel,
                selectedTabTextColor: Colors.white,
                squeezeIntensity: 2,
                height: 40,
                tabPadding: EdgeInsets.symmetric(horizontal: 8),
                textStyle: TextStyle(
                  color: ThemeColor.tabLabel,
                  fontSize: 14,
                ),
                tabs: [
                  SegmentTab(
                    label: 'Arbeitnehmer',
                  ),
                  SegmentTab(
                    label: 'Arbeitgeber',
                  ),
                  SegmentTab(
                    label: 'Temporärbüro',
                  ),
                ],
              ),
            ),
            // Sample pages
            Padding(
              padding: const EdgeInsets.only(top: 70),
              child: TabBarView(
                physics: const BouncingScrollPhysics(),
                children: [
                  TabWidget(
                    index: 1,
                    img: LocalImage.tab1MobImg,
                    text: UsedText.tabData1,
                  ),
                  TabWidget(
                    index: 1,
                    img: LocalImage.tab2MobImg,
                    text: UsedText.tabData2,
                  ),
                  TabWidget(
                    index: 3,
                    img: LocalImage.tab3MobImg,
                    text: UsedText.tabData3,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
