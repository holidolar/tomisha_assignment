import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/mobile/tab_widgets/tab_widget_one.dart';
import 'package:tomisha/home_page/mobile/tab_widgets/tab_widget_three.dart';
import 'package:tomisha/home_page/mobile/tab_widgets/tab_widget_two.dart';

class TabWidget extends StatelessWidget {
  const TabWidget({
    Key? key,
    required this.index,
    required this.text,
    required this.img,
  }) : super(key: key);

  final int index;
  final List<String> text;
  final List<String> img;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          const SizedBox(height: 25),
          SizedBox(
            width: 280,
            child: Text(
              text[0],
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: ThemeColor.inTabHeading,
                fontSize: 21,
              ),
            ),
          ),
          TabWidgetOne(
            img: img,
            text: text,
            index: 1,
          ),
          TabWidgetTwo(
            img: img,
            text: text,
            index: 2,
          ),
          TabWidgetThree(
            img: img,
            text: text,
            index: 3,
          ),
        ],
      ),
    );
  }
}
