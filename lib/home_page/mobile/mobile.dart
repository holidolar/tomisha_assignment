import 'package:flutter/material.dart';
import 'package:tomisha/common_widgets/button.dart';
import 'package:tomisha/home_page/mobile/tab_bar_info.dart';
import 'package:tomisha/home_page/mobile/mobile_header.dart';

class MobileHolder extends StatelessWidget {
  const MobileHolder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const MobileHeader(),
            InfoTabMiddleSection(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10.0),
            topLeft: Radius.circular(10.0),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.brown,
              blurRadius: 5,
            ),
          ],
        ),
        height: 100,
        width: double.infinity,
        child: const Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 24.0,
          ),
          child: MainButton(),
        ),
      ),
    );
  }
}
