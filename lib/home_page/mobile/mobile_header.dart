import 'package:flutter/material.dart';
import 'package:tomisha/common_widgets/button.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MobileHeader extends StatelessWidget {
  const MobileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 600,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: ThemeColor.headerGradient,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 15.sp),
              const Text(
                'Deine Job\nwebsite',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 42,
                  fontWeight: FontWeight.w600,
                  color: ThemeColor.heading,
                  letterSpacing: 1.26,
                ),
              ),
              SizedBox(height: 20.sp),
              Image.asset(
                LocalImage.handShake,
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
