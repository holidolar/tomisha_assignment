import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tomisha/common_widgets/button.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/controller/widget_view_controller.dart';

class Navbar extends StatelessWidget {
  Navbar({Key? key}) : super(key: key);

  final WidgetViewController ctrl = Get.find();

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return AppBar(
      elevation: 2,
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(12),
        ),
      ),
      flexibleSpace: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Obx(
              () {
                if (_width > Dimension.mobileWidth) {
                  return ctrl.isVisible.value
                      ? const SizedBox.shrink()
                      : const MainButton(
                          isWeb: true,
                        );
                } else {
                  return const SizedBox.shrink();
                }
              },
            ),
            const SizedBox(width: 4 * Dimension.spaceX),
            const Text(
              'Login',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: ThemeColor.green,
              ),
            ),
            const SizedBox(width: 4 * Dimension.spaceX),
          ],
        ),
      ),
    );
  }
}
