import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/controller/widget_view_controller.dart';
import 'package:visibility_detector/visibility_detector.dart';

class Header extends StatelessWidget {
  final WidgetViewController ctrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      decoration: const BoxDecoration(
        gradient: ThemeColor.headerGradient,
      ),
      child: Row(
        children: [
          SizedBox(width: 30.sp),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Deine Job\nwebsite',
                style: TextStyle(
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w600,
                  color: ThemeColor.heading,
                ),
              ),
              SizedBox(height: 20.sp),
              VisibilityDetector(
                key: const Key("unique key"),
                onVisibilityChanged: (VisibilityInfo info) {
                  debugPrint("${info.visibleFraction} of my widget is visible");
                  if (info.visibleFraction == 0) {
                    ctrl.isVisible(false);
                  } else {
                    ctrl.isVisible(true);
                  }
                },
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    width: 90.sp,
                    height: 40.0,
                    decoration: BoxDecoration(
                      gradient: ThemeColor.btnGradient,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Center(
                      child: Text(
                        'Kostenlos Registrieren',
                        style: TextStyle(
                          fontSize: 5.sp,
                          color: ThemeColor.btn,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 20.sp),
          Container(
            height: 120.sp,
            width: 120.sp,
            decoration: const BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: Image.asset(
              LocalImage.handShake,
              fit: BoxFit.scaleDown,
            ),
          )
        ],
      ),
    );
  }
}
