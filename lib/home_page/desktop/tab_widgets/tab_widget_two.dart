import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/mobile/common/desciption_text.dart';
import 'package:tomisha/home_page/mobile/common/heighlight_number.dart';

class TabWidgetTwo extends StatelessWidget {
  const TabWidgetTwo({
    Key? key,
    required this.img,
    required this.text,
    required this.index,
  }) : super(key: key);

  final List<String> img;
  final List<String> text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: ThemeColor.headerGradient,
      ),
      width: double.infinity,
      height: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              img[index - 1],
            ),
          ),
          SizedBox(width: 30.sp),
          Positioned(
            top: 5,
            left: 30,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                HighlightedNumber(
                  number: index,
                ),
                Positioned(
                  bottom: 27,
                  left: 120,
                  child: DescriptionText(
                    text: text[index],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
