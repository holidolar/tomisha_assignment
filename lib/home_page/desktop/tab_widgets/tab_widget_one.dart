import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/desciption_text.dart';
import 'package:tomisha/home_page/mobile/common/heighlight_number.dart';

class TabWidgetOne extends StatelessWidget {
  const TabWidgetOne({
    Key? key,
    required this.img,
    required this.text,
    required this.index,
  }) : super(key: key);

  final List<String> img;
  final List<String> text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 300,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  const Positioned(
                    left: -50,
                    top: -10,
                    child: CircleWidget(size: 200),
                  ),
                  Stack(
                    clipBehavior: Clip.none,
                    children: [
                      HighlightedNumber(
                        number: index,
                      ),
                      Positioned(
                        bottom: 27,
                        left: 120,
                        child: DescriptionText(
                          text: text[index],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(width: 70.sp),
              Image.asset(
                img[index - 1],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
