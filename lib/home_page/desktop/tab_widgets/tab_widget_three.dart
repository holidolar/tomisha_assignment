import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/mobile/common/circle_widget.dart';
import 'package:tomisha/home_page/mobile/common/desciption_text.dart';
import 'package:tomisha/home_page/mobile/common/heighlight_number.dart';
// TabWidgetThree

class TabWidgetThree extends StatelessWidget {
  const TabWidgetThree({
    Key? key,
    required this.img,
    required this.text,
    required this.index,
  }) : super(key: key);

  final List<String> img;
  final List<String> text;
  final int index;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              const CircleWidget(
                size: 150,
              ),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  HighlightedNumber(
                    number: index,
                  ),
                  Positioned(
                    bottom: 55,
                    left: 120,
                    child: DescriptionText(
                      text: text[index],
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(width: 50.sp),
          Image.asset(
            img[index - 1],
          ),
        ],
      ),
    );
  }
}
