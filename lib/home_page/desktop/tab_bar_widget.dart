import 'package:animated_segmented_tab_control/animated_segmented_tab_control.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:tomisha/home_page/desktop/tab_widget.dart';
import 'package:tomisha/home_page/desktop/tab_widgets/tab_widget_one.dart';
import 'package:tomisha/home_page/desktop/tab_widgets/tab_widget_three.dart';
import 'package:tomisha/home_page/desktop/tab_widgets/tab_widget_two.dart';

class TabBarWidget extends StatelessWidget {
  const TabBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: SizedBox(
              width: 150.sp,
              child: const SegmentedTabControl(
                radius: Radius.circular(10),
                backgroundColor: Colors.white,
                indicatorColor: ThemeColor.tabActive,
                tabTextColor: ThemeColor.tabLabel,
                selectedTabTextColor: Colors.white,
                squeezeIntensity: 2,
                height: 40,
                tabPadding: EdgeInsets.symmetric(horizontal: 8),
                textStyle: TextStyle(
                  color: ThemeColor.tabLabel,
                  fontSize: 14,
                ),
                tabs: [
                  SegmentTab(
                    label: 'Arbeitnehmer',
                  ),
                  SegmentTab(
                    label: 'Arbeitgeber',
                  ),
                  SegmentTab(
                    label: 'Temporärbüro',
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 1200,
            child: TabBarView(
              physics: const BouncingScrollPhysics(),
              children: [
                TabWidget(
                  index: 1,
                  img: LocalImage.tab1WebImg,
                  text: UsedText.tabData1,
                ),
                TabWidget(
                  index: 2,
                  img: LocalImage.tab2WebImg,
                  text: UsedText.tabData2,
                ),
                TabWidget(
                  index: 3,
                  img: LocalImage.tab3WebImg,
                  text: UsedText.tabData3,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
