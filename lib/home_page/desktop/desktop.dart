import 'package:flutter/material.dart';
import 'package:tomisha/home_page/desktop/header.dart';
import 'package:tomisha/home_page/desktop/tab_bar_widget.dart';

class DesktopHolder extends StatelessWidget {
  const DesktopHolder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Header(),
          const TabBarWidget(),
        ],
      ),
    );
  }
}
