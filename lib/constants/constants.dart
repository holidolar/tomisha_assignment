import 'package:flutter/material.dart';

class Dimension {
  static const spaceX = 4.0;
  static const mobileWidth = 600;
}

class ThemeColor {
  static const green = Color(0xff319795);
  static const heading = Color(0xff2D3748);
  static const btn = Color(0xffE6FFFA);
  static const tabLabel = Color(0xff319795);
  static const tabActive = Color(0xff81E6D9);
  static const inTabHeading = Color(0xff4A5568);
  static const highlightedText = Color(0xff718096);

  /// gradients
  static const headerGradient = LinearGradient(
    colors: [
      Color(0xffEBF4FF),
      Color(0xffE6FFFA),
    ],
  );

  static const btnGradient = LinearGradient(
    colors: [
      Color(0xff319795),
      Color(0xff3182CE),
    ],
  );
}

class LocalImage {
  static const handShake = "assets/undraw_agreement_aajr.png";

  static List<String> tab1MobImg = [
    "assets/tab_one/mob/1.png",
    "assets/tab_one/mob/2.png",
    "assets/tab_one/mob/3.png",
  ];

  static List<String> tab1WebImg = [
    "assets/tab_one/web/1.png",
    "assets/tab_one/web/2.png",
    "assets/tab_one/web/3.png",
  ];

  static List<String> tab2MobImg = [
    "assets/tab_one/mob/1.png",
    "assets/tab_two/mob/2.png",
    "assets/tab_two/mob/3.png",
  ];

  static List<String> tab2WebImg = [
    "assets/tab_one/web/1.png",
    "assets/tab_two/web/2.png",
    "assets/tab_two/web/3.png",
  ];

  static List<String> tab3MobImg = [
    "assets/tab_one/mob/1.png",
    "assets/tab_three/mob/2.png",
    "assets/tab_three/mob/3.png",
  ];

  static List<String> tab3WebImg = [
    "assets/tab_one/web/1.png",
    "assets/tab_three/web/2.png",
    "assets/tab_three/web/3.png",
  ];
}

class UsedText {
  static List<String> tabData1 = [
    'Drei einfache Schritte zu deinem neuen Job',
    'Erstellen dein Lebenslauf',
    'Erstellen dein Lebenslauf',
    'Mit nur einem Klick\nbewerben',
  ];

  static List<String> tabData2 = [
    'Drei einfache Schritte zu deinem neuen Mitarbeiter',
    'Erstellen dein Unternehmensprofil',
    'Erstellen ein Jobinserat',
    'Wähle deinen neuen Mitarbeiter aus',
  ];

  static List<String> tabData3 = [
    'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter',
    'Erstellen dein Unternehmensprofil',
    'Erhalte Vermittlungs- angebot\nvon Arbeitgeber',
    'Vermittlung nach Provision\noder Stundenlohn',
  ];
}
