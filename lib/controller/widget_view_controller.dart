import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WidgetViewController extends GetxController {
  var isVisible = true.obs;

  void onInit() {
    super.onInit();
    ever(isVisible, (val) {
      debugPrint(val.toString());
    });
  }
}
