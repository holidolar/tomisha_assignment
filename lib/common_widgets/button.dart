import 'package:flutter/material.dart';
import 'package:tomisha/constants/constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainButton extends StatelessWidget {
  const MainButton({
    Key? key,
    this.isWeb = false,
  }) : super(key: key);
  final isWeb;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        width: isWeb ? 250 : 320,
        height: isWeb ? 30 : 40,
        decoration: BoxDecoration(
          gradient: ThemeColor.btnGradient,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Center(
          child: Text(
            'Kostenlos Registrieren',
            style: TextStyle(
              fontSize: isWeb ? 5.sp : 15.sp,
              color: ThemeColor.btn,
            ),
          ),
        ),
      ),
    );
  }
}
