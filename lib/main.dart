import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tomisha/home_page/mobile/mobile.dart';
import 'package:tomisha/home_page/nav_bar.dart';
import 'package:tomisha/home_page/desktop/desktop.dart';
import 'package:tomisha/layout/responsive_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'controller/widget_view_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HolderWidget(),
    );
  }
}

class HolderWidget extends StatelessWidget {
  HolderWidget({
    Key? key,
  }) : super(key: key);

  final controller = Get.put(WidgetViewController());

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (BuildContext context, Widget? child) => Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: Navbar(),
        ),
        body: ResponsiveLayout(
          desktopBody: const DesktopHolder(),
          mobileBody: const MobileHolder(),
        ),
      ),
    );
  }
}
